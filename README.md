# HomeWorld

### General Description
HomeWorld is a single-page application that allows the users to upload photos and share them as public for everyone or private only for their followers. Users are able to review their posts, to delete them or change their privacy, to update their profile or request delete of the account. They can also view, comment and like the posts shared by others. They are able to review their followers and the people they follow and the posts they liked. Users can receive notifications if someone followed them or liked their post.

 <a target="_blank" href="./screenshots/register.png"><img width="19%" src="./screenshots/register.png"  title="register"></a> <a target="_blank" href="./screenshots/login.png"><img width="19%" src="./screenshots/login.png"  title="login"></a>  <a target="_blank" href="./screenshots/home.png"><img width="19%" src="./screenshots/home.png"  title="home"></a>   <a target="_blank" href="./screenshots/users.png"><img width="19%" src="./screenshots/users.png"  title="users"></a>  <a target="_blank" href="./screenshots/profile.png"><img width="19%" src="./screenshots/profile.png"  title="profile"></a>

---

### Installation & Running

#### Server

After you clone successfully this repository:

- navigate to the `server` folder
- create a **database** in MySQL Workbench. The default name is *homeworlddb*, rename it if you want, but it has to be the same in the .env config. In order to be able to use cyrilic fonts in the application, please select utf8mb4 charset with default collation.

- create `.env` file at root level which include sensitive information for your server:

  ``` sh
  PORT=3000
  DB_TYPE=mysql
  DB_HOST=localhost
  DB_PORT=3306
  DB_USERNAME=root
  DB_PASSWORD='root'
  DB_DATABASE_NAME=homeworlddb
  JWT_SECRET='jwt-secret'
  JWT_EXPIRE:604800000
  CLIENT_ID='Client-ID e8c08597e538770'
  PHOTO_STORAGE_URL='https://api.imgur.com/3'
  ```

- open the terminal and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  $ npm run start
  ```

  ```sh
  $ npm run seed
  ```
#### Client
- navigate to the `client` folder.
- open the terminal and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  $ ng serve --o
  ```

---

### Built With

 - [Angular 7](https://angular.io/) - framework used for our client.
 - [NestJS](https://nestjs.com/) - framework for building our server.
 - [Bootstrap](https://ng-bootstrap.github.io/) - to design our components in the client.
 - [TypeORM](http://typeorm.io/#/) - an **ORM** that can run in **Node.js**;
 - [JWT](https://jwt.io/) - for authentication.

---

### Authors and Contributors

- [Zdravko Verguilov](https://gitlab.com/verguilov)
- [Nikolay Neykov](https://gitlab.com/nikolayneykov92)

---

### License

This project is licensed under **[MIT](https://choosealicense.com/licenses/mit/)** License
