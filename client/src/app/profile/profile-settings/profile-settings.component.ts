import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDTO } from '../../models/user/user.dto';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent {
  profileSettingsForm: FormGroup;
  photo: any;
  photoUrl: any;

  @Input() loggedUserData: UserDTO;
  @Input() user: UserDTO;
  @Output() userDelete: EventEmitter<void> = new EventEmitter<void>();
  @Output() userUpdate: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(
    private readonly fb: FormBuilder,
  ) {
    this.buildForm();
  }

  updateUser(): void {
    this.userUpdate.emit({ ...this.profileSettingsForm.value, photo: this.photo });
    this.profileSettingsForm.reset();
    this.photo = null;
  }

  onPhotoChange(event: any): void {
    const file = event.target.files[0];

    if (!file || !(/image\/(gif|jpg|jpeg|png)$/i).test(file.type)) {
      return;
    }

    this.photo = file;
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = () => {
      this.photoUrl = reader.result;
    };
  }

  deleteUser() {
    this.userDelete.emit();
  }

  private buildForm(): void {
    this.profileSettingsForm = this.fb.group({
      newUsername: ['', Validators.compose([
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
      newEmail: ['', Validators.compose([
        Validators.email,
        Validators.maxLength(30),
      ])],
      newPassword: ['', Validators.compose([
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
      repeatNewPassword: ['', Validators.compose([
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
    },
      { validator: [this.checkChanges, this.checkPasswords] },
    );
  }

  private checkChanges(group: FormGroup) {
    const newUsername: string = group.get('newUsername').value;
    const newEmail: string = group.get('newEmail').value;
    const newPassword: string = group.get('newPassword').value;

    return (newUsername || newEmail || newPassword) ? null : { noChanges: true };
  }

  private checkPasswords(group: FormGroup) {
    const newPassword = group.get('newPassword').value;
    const repeatNewPassword = group.get('repeatNewPassword').value;

    return newPassword === repeatNewPassword ? null : { notSame: true };
  }
}
