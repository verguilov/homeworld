import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserDTO } from './../../models/user/user.dto';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-update-profile-confirmation',
  templateUrl: './update-profile-confirmation.component.html',
  styleUrls: ['./update-profile-confirmation.component.scss']
})
export class UpdateProfileConfirmationComponent {
  confirmationForm: FormGroup;
  @Input() message: string;
  @Input() userToUpdate: UserDTO;
  @Output() confirm: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private readonly fb: FormBuilder,
  ) {
    this.confirmationForm = this.fb.group({
      currentPassword: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ])]
    });
  }

  accept(): void {
    this.confirm.emit(this.confirmationForm.controls.currentPassword.value);
  }

  deny(): void {
    this.confirm.emit(null);
  }
}
