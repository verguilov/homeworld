import { PostsModule } from './../posts/posts.module';
import { UsersModule } from './../users/users.module';
import { ProfileRoutingModule } from './profile.routing';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { UpdateProfileConfirmationComponent } from './update-profile-confirmation/update-profile-confirmation.component';
import { FollowersComponent } from './followers/followers.component';
import { FollowingComponent } from './following/following.component';

@NgModule({
  declarations: [ProfileComponent, ProfileSettingsComponent, UpdateProfileConfirmationComponent, FollowersComponent, FollowingComponent],
  imports: [
    SharedModule,
    UsersModule,
    ProfileRoutingModule,
    PostsModule,
  ],
  entryComponents: [
    UpdateProfileConfirmationComponent,
  ]
})
export class ProfileModule { }
