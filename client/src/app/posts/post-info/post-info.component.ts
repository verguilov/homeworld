import { PostDTO } from './../../models/post/post.dto';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post-info',
  templateUrl: './post-info.component.html',
  styleUrls: ['./post-info.component.scss']
})
export class PostInfoComponent {
  @Input() post: PostDTO;

  @Output() postDetailsShow: EventEmitter<PostDTO> = new EventEmitter<PostDTO>();

  showPostDetails(): void {
    this.postDetailsShow.emit(this.post);
  }
}
