import { CommentDTO } from '../../models/comment/comment.dto';
import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
})
export class CommentEditComponent {
  @Input() commentToEdit: CommentDTO;
  @Output() commentEdit: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();

  editComment(): void {
    this.commentEdit.emit(this.commentToEdit);
  }
}
