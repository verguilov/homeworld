import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from './../../core/services/notification.service';
import { Component, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostsService } from '../../core/services/posts.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent {
  @Output() postCreate: EventEmitter<FormData> = new EventEmitter<FormData>();

  createPostForm: FormGroup;
  photo: any;
  photoUrl: any;
  location: any;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly fb: FormBuilder,
    private readonly postsService: PostsService,
    private readonly notificationService: NotificationService,
    private readonly spinner: NgxSpinnerService,
  ) {
    this.buildForm();
    this.spinner.show('location-spinner');
    this.postsService
      .getLocation()
      .pipe(delay(1500))
      .subscribe((location) => {
        this.location = location;
        this.spinner.hide('location-spinner');
      });
  }

  createPost(): void {
    const formData: FormData = new FormData();
    formData.append('title', this.createPostForm.get('title').value);
    formData.append('description', this.createPostForm.get('description').value);
    formData.append('status', this.createPostForm.get('status').value);
    formData.append('photo', this.photo);
    formData.append('location', `${this.location.city}, ${this.location.country_name}`);

    this.postCreate.emit(formData);
  }

  onPhotoChange(event: any): void {
    const file = event.target.files[0];

    if (!file || !(/image\/(gif|jpg|jpeg|png)$/i).test(file.type)) {
      this.notificationService.warning(`Not supported image format! The supported image formats are: gif, jpg, jpeg and png.`);
      return;
    }

    if (file.size > 2000000) {
      this.notificationService.warning(`Maximum image size is 2mb!`);
      return;
    }

    this.photo = file;
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = () => {
      this.photoUrl = reader.result;
    };
  }

  private buildForm(): void {
    this.createPostForm = this.fb.group({
      title: ['', Validators.compose([
        Validators.required,
      ])],
      description: ['', Validators.compose([
        Validators.required,
      ])],
      status: ['public', Validators.compose([
        Validators.required,
      ])],
    },
    );
  }
}
