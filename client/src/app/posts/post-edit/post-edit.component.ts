import { PostDTO } from './../../models/post/post.dto';
import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss']
})
export class PostEditComponent implements OnInit {
  @Input() post: PostDTO;
  @Output() postEdit: EventEmitter<PostDTO> = new EventEmitter<PostDTO>();

  editPostForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
  ) {
    this.buildForm();
  }

  updatePost(): void {
    this.postEdit.emit(this.editPostForm.value);
  }


  private buildForm(): void {
    this.editPostForm = this.fb.group({
      title: ['', Validators.compose([
        Validators.required,
      ])],
      description: ['', Validators.compose([
        Validators.required,
      ])],
      status: ['', Validators.compose([
        Validators.required,
      ])],
    },
    );
  }

  ngOnInit(): void {
    this.editPostForm.controls.title.setValue(this.post.title);
    this.editPostForm.controls.description.setValue(this.post.description);
    this.editPostForm.controls.status.setValue(this.post.status);
  }
}
