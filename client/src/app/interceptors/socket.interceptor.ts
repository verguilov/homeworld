import { SocketService } from './../core/services/socket.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../core/services/storage.service';

@Injectable()
export class SocketInterceptor implements HttpInterceptor {
  constructor(
    private readonly storage: StorageService,
    private readonly socketService: SocketService,
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const username = this.storage.getItem('username');
    const joined = this.storage.getItem('joined');

    if (joined && username) {
      this.socketService.join(username);
    }

    return next.handle(request.clone());
  }
}
