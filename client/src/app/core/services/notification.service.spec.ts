import { NotificationService } from './notification.service';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { TestBed } from '@angular/core/testing';

describe('NotificationSerice', () => {
  const fakeMessage = 'test message';
  const fakeTitle = 'test title';

  let toastr;
  let notificationService: NotificationService;

  beforeEach(async () => {
    toastr = {
      success() { },
      error() { },
      warning() { },
      info() { },
    };

    TestBed.configureTestingModule({
      imports: [
        ToastrModule
      ],
      providers: [NotificationService]
    })
      .overrideProvider(ToastrService, { useValue: toastr });

    notificationService = TestBed.get(ToastrService);
  });

  it('should be created', () => {
    expect(notificationService).toBeTruthy();
  });

  it('notificationService.success() should call toastr.success() with the correct parameters', () => {
    const successSpy = spyOn(toastr, 'success');

    notificationService.success(fakeMessage, fakeTitle);

    expect(successSpy).toHaveBeenCalledTimes(1);
    expect(successSpy).toHaveBeenCalledWith(fakeMessage, fakeTitle);
  });

  it('notificationService.error() should call toastr.error() with the correct parameters', () => {
    const errorSpy = spyOn(toastr, 'error');

    notificationService.error(fakeMessage, fakeTitle);

    expect(errorSpy).toHaveBeenCalledTimes(1);
    expect(errorSpy).toHaveBeenCalledWith(fakeMessage, fakeTitle);
  });

  it('notificationService.warning() should call toastr.warning() with the correct parameters', () => {
    const warningSpy = spyOn(toastr, 'warning');

    notificationService.warning(fakeMessage, fakeTitle);

    expect(warningSpy).toHaveBeenCalledTimes(1);
    expect(warningSpy).toHaveBeenCalledWith(fakeMessage, fakeTitle);
  });

  it('notificationService.info() should call toastr.info() with the correct parameters', () => {
    const infoSpy = spyOn(toastr, 'info');

    notificationService.info(fakeMessage, fakeTitle);

    expect(infoSpy).toHaveBeenCalledTimes(1);
    expect(infoSpy).toHaveBeenCalledWith(fakeMessage, fakeTitle);
  });
});
