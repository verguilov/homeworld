import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CommentCreateDTO } from '../../models/comment/comment-create.dto';

@Injectable()
export class CommentsService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  findComments(postId: number): Observable<any> {
    return this.http.get<any>(`${API_URL}/posts/${postId}/comments`);
  }

  createComment(postId: number, comment: CommentCreateDTO): Observable<any> {
    return this.http.post<any>(`${API_URL}/posts/${postId}/comments`, comment);
  }

  editComment(postId: number, commentId: number, comment: CommentCreateDTO): Observable<any> {
    return this.http.put<any>(`${API_URL}/posts/${postId}/comments/${commentId}`, comment);
  }

  deleteComment(postId: number, commentId: number): Observable<any> {
    return this.http.delete<any>(`${API_URL}/posts/${postId}/comments/${commentId}`);
  }

  likeComment(postId: number, commentId: number, like: any): Observable<any> {
    return this.http.post<any>(`${API_URL}/posts/${postId}/comments/${commentId}/likes`, like);
  }
}
