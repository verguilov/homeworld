import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  setItem(key: string, value: any) {
    localStorage.setItem(key, String(value));
  }

  getItem(key: string) {
    const value = localStorage.getItem(key);

    return value && value !== 'undefined'
      ? value
      : null;
  }

  removeItem(key: string) {
    localStorage.removeItem(key);
  }

  clear() {
    localStorage.clear();
  }
}
