import { AuthService } from './auth.service';
import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../../models/user/user.dto';
import { StorageService } from './storage.service';
import { tap } from 'rxjs/operators';
import { PostDTO } from '../../models/post/post.dto';

@Injectable()
export class UsersService {
   constructor(
      private readonly http: HttpClient,
      private readonly storageService: StorageService,
      private readonly authService: AuthService,
   ) { }

   findUsers(keyword: string, skip: number, take: number): Observable<UserDTO[]> {
      return this.http
         .get<UserDTO[]>(`${API_URL}/users?keyword=${keyword}&take=${take}&skip=${skip}`);
   }

   registerUser(user: UserRegisterDTO): Observable<UserDTO> {
      return this.http.post<UserDTO>(`${API_URL}/users`, user)
         .pipe(
            tap(({ username, email }) => {
               this.storageService.setItem('email', email);
               this.storageService.setItem('username', username);
            })
         );
   }

   deleteUser(userId: number): Observable<UserDTO> {
      return this.http.delete<UserDTO>(`${API_URL}/users/${userId}`)
         .pipe(
            tap(() => {
               this.authService.logout();
               this.storageService.clear();
            })
         );
   }

   updateUser(userId: number, formData: FormData): Observable<UserDTO> {
      return this.http.put<UserDTO>(`${API_URL}/users/${userId}`, formData);
   }

   findUserById(userId: number): Observable<UserDTO> {
      return this.http.get<UserDTO>(`${API_URL}/users/${userId}`);
   }

   followUser(
      userId: number, followingUserId: number
   ): Observable<{ userFollower: UserDTO, userFollowed: UserDTO }> {
      return this.http.post<{ userFollower: UserDTO, userFollowed: UserDTO }>(`${API_URL}/users/${userId}/following`, {
         userId: followingUserId,
         follow: true
      });
   }

   unFollowUser(
      userId: number, followingUserId: number
   ): Observable<{ userFollower: UserDTO, userFollowed: UserDTO }> {
      return this.http.post<{ userFollower: UserDTO, userFollowed: UserDTO }>(`${API_URL}/users/${userId}/following`, {
         userId: followingUserId,
         follow: false
      });
   }

   findUserFollowers(userId: number): Observable<UserDTO[]> {
      return this.http.get<UserDTO[]>(`${API_URL}/users/${userId}/following`);
   }

   findUserFollowing(userId: number): Observable<UserDTO[]> {
      return this.http.get<UserDTO[]>(`${API_URL}/users/${userId}/followers`);
   }

   findUserCreatedPosts(userId: number): Observable<PostDTO[]> {
      return this.http.get<PostDTO[]>(`${API_URL}/users/${userId}/posts`);
   }

   findUserLikedPosts(userId: number): Observable<PostDTO[]> {
      return this.http.get<PostDTO[]>(`${API_URL}/users/${userId}/posts/liked`);
   }
}
