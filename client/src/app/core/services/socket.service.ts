import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable()
export class SocketService {
  constructor(
    private readonly socket: Socket,
  ) { }

  join(username: string): void {
    this.socket.emit('join', username);
  }

  leave(username: string): void {
    this.socket.emit('leave', username);
  }

  sendNotification(username: string): void {
    this.socket.emit('notification', username);
  }

  receiveNotification() {
    return this.socket.fromEvent('notification');
  }
}
