import { UserLoginDTO } from './../../models/user/user-login.dto';
import { SocketIoModule } from 'ngx-socket-io';
import { SocketService } from './socket.service';
import { async, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './storage.service';
import { of } from 'rxjs';

describe('AuthService', () => {
  const fakeToken = 'authToken';
  let httpClient;
  let storageService;
  let jwtService;
  let socketService;
  let authService: AuthService;


  beforeEach(async () => {
    httpClient = {
      post() { },
      delete() { }
    };

    storageService = {
      getItem() { return fakeToken; },
      setItem() { },
      removeItem() { }
    };

    jwtService = {
      isTokenExpired() { },
      decodeToken() { }
    };

    socketService = {
      join() { },
      leave() { },
    };


    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        JwtModule.forRoot({ config: {} }),
        SocketIoModule.forRoot({
          url: '',
          options: {},
        }),
      ],
      providers: [AuthService, StorageService, SocketService]
    })
      .overrideProvider(HttpClient, { useValue: httpClient })
      .overrideProvider(StorageService, { useValue: storageService })
      .overrideProvider(JwtHelperService, { useValue: jwtService })
      .overrideProvider(SocketService, { useValue: socketService });

    authService = TestBed.get(AuthService);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  describe('login() should', () => {
    it('call http.post with the correct parameters', async (done) => {
      spyOn(authService, 'getLoggedUserData').and.returnValue('john_doe');

      const mockUser: UserLoginDTO = {
        email: 'john_doe@email.com',
        password: '123',
      };

      spyOn(httpClient, 'post').and.callFake(() => of({ fakeToken }));

      authService.login(mockUser).subscribe(() => {
        expect(httpClient.post).toHaveBeenCalledTimes(1);
        expect(httpClient.post).toHaveBeenCalledWith('http://localhost:3000/api/session', mockUser);
      });

      done();
    });

    it('call storageService.setItem() with correct parameters', async (done) => {
      spyOn(httpClient, 'post').and.callFake(() => of({ fakeToken }));
      const mockUser: UserLoginDTO = { username: 'john_doe', email: 'john_doe@email.com', password: '123' };
      spyOn(authService, 'getLoggedUserData').and.returnValue(mockUser);
      const setItemSpy = spyOn(storageService, 'setItem');

      authService.login(mockUser).subscribe(
        ({ authToken }) => {
          expect(setItemSpy).toHaveBeenCalledWith('authToken', authToken);
          expect(setItemSpy).toHaveBeenCalledWith('username', mockUser.username);
          expect(setItemSpy).toHaveBeenCalledWith('email', mockUser.email);
          expect(setItemSpy).toHaveBeenCalledWith('joined', true);
        }
      );

      done();
    });

    it('call storageService.getItem()', async (done) => {
      spyOn(authService, 'getLoggedUserData').and.returnValue('john_doe');
      spyOn(httpClient, 'post').and.callFake(() => of({ fakeToken }));
      const mockUser: UserLoginDTO = { username: 'john_doe', password: '123' };
      const getItemSpy = spyOn(storageService, 'getItem').and.returnValue('test');

      authService.login(mockUser).subscribe(
        ({ authToken }) => {
          expect(getItemSpy).toHaveBeenCalledTimes(1);
        }
      );

      done();
    });

    it('call socketService.join() with correct parameter', async (done) => {
      spyOn(authService, 'getLoggedUserData').and.returnValue('john_doe');
      spyOn(httpClient, 'post').and.callFake(() => of({ fakeToken }));
      spyOn(storageService, 'getItem').and.returnValue('john_doe');
      const mockUser: UserLoginDTO = { username: 'john_doe', password: '123' };
      const joinSpy = spyOn(socketService, 'join');

      authService.login(mockUser).subscribe(
        ({ authToken }) => {
          expect(joinSpy).toHaveBeenCalledTimes(1);
          expect(joinSpy).toHaveBeenCalledWith('john_doe');
        }
      );

      done();
    });
  });

  describe('logout() should', () => {
    it('calls storageService.removeItem() with correct parameter', () => {
      const removeItemSpy = spyOn(storageService, 'removeItem');

      authService.logout();

      expect(removeItemSpy).toHaveBeenCalledTimes(1);
      expect(removeItemSpy).toHaveBeenCalledWith('authToken');
    });

    it('calls storageService.setItem() with correct parameters', () => {
      const setItemSpy = spyOn(storageService, 'setItem');

      authService.logout();

      expect(setItemSpy).toHaveBeenCalledTimes(1);
      expect(setItemSpy).toHaveBeenCalledWith('joined', false);
    });

    it('call socketService.leave() with correct parameter', () => {
      spyOn(storageService, 'getItem').and.returnValue('john_doe');
      const leaveSpy = spyOn(socketService, 'leave');

      authService.logout();

      expect(leaveSpy).toHaveBeenCalledTimes(1);
      expect(leaveSpy).toHaveBeenCalledWith('john_doe');
    });

    it('call storageService.getItem() with correct parameter', () => {
      const getItemSpy = spyOn(storageService, 'getItem');

      authService.logout();

      expect(getItemSpy).toHaveBeenCalledTimes(1);
      expect(getItemSpy).toHaveBeenCalledWith('username');
    });
  });

  describe('getLoggedUserData() should', () => {
    it('call the storageService.getItem() method once with correct parameter', () => {
      const getItemSpy = spyOn(storageService, 'getItem');

      authService.getLoggedUserData();

      expect(getItemSpy).toHaveBeenCalledTimes(1);
      expect(getItemSpy).toHaveBeenCalledWith(fakeToken);
    });

    it('call the jwtService.isTokenExpired() method once with correct token if the token exists', () => {
      const isTokenExpiredSpy = spyOn(jwtService, 'isTokenExpired');

      authService.getLoggedUserData();

      expect(isTokenExpiredSpy).toHaveBeenCalledTimes(1);
      expect(isTokenExpiredSpy).toHaveBeenCalledWith(fakeToken);
    });

    it('call the storageService.removeItem() method once with correct parameter if the token exists as is expired', () => {
      const removeItemSpy = spyOn(storageService, 'removeItem');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(true);

      authService.getLoggedUserData();

      expect(removeItemSpy).toHaveBeenCalledTimes(1);
      expect(removeItemSpy).toHaveBeenCalledWith(fakeToken);
    });

    it('call the storageService.setItem() method once with correct parameters if the token exists as is expired', () => {
      const setItemSpy = spyOn(storageService, 'setItem');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(true);

      authService.getLoggedUserData();

      expect(setItemSpy).toHaveBeenCalledTimes(1);
      expect(setItemSpy).toHaveBeenCalledWith('joined', false);
    });

    it('should call the jwtService.decodeToken() method once with correct parameter if the token exists as is not expired', () => {
      const decodeTokenSpy = spyOn(jwtService, 'decodeToken');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(false);

      authService.getLoggedUserData();

      expect(decodeTokenSpy).toHaveBeenCalledTimes(1);
      expect(decodeTokenSpy).toHaveBeenCalledWith(fakeToken);
    });

    it('should return the value of jwtService.decodeToken() if the token exists as is not expired', () => {
      spyOn(jwtService, 'decodeToken').and.returnValue('decoded token');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(false);

      const decodedToken = authService.getLoggedUserData();

      expect(decodedToken).toEqual('decoded token');
    });

    it('should return null if the token does not exist', () => {
      spyOn(storageService, 'getItem').and.returnValue(null);
      spyOn(jwtService, 'decodeToken').and.returnValue('decoded token');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(false);

      const decodedToken = authService.getLoggedUserData();

      expect(decodedToken).toBeNull();
    });

    it('should return null if the token exists but is expired', () => {
      spyOn(jwtService, 'decodeToken').and.returnValue('decoded token');
      spyOn(jwtService, 'isTokenExpired').and.returnValue(true);

      const decodedToken = authService.getLoggedUserData();

      expect(decodedToken).toBeNull();
    });
  });
});
