import { StorageService } from './storage.service';
import { TestBed } from '@angular/core/testing';

describe('SocketService', () => {
  const fakeKey = 'test key';
  const fakeValue = 'test value';
  let storageService: StorageService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [StorageService]
    });

    storageService = TestBed.get(StorageService);
  });

  it('should be created', () => {
    expect(storageService).toBeTruthy();
  });

  it('storageService.setItem() should call localStorage.setItem() with correct parameters', () => {
    const setItemSpy = spyOn(localStorage, 'setItem');

    storageService.setItem(fakeKey, fakeValue);

    expect(setItemSpy).toHaveBeenCalledTimes(1);
    expect(setItemSpy).toHaveBeenCalledWith(fakeKey, fakeValue);
  });

  it('storageService.getItem() should call localStorage.getItem() with correct parameters', () => {
    const getItemSpy = spyOn(localStorage, 'getItem');

    storageService.getItem(fakeKey);

    expect(getItemSpy).toHaveBeenCalledTimes(1);
    expect(getItemSpy).toHaveBeenCalledWith(fakeKey);
  });

  it('storageService.getItem() should return null if there is not stored data', () => {
    spyOn(localStorage, 'getItem');

    const item = storageService.getItem(fakeKey);

    expect(item).toBeNull();
  });

  it('storageService.getItem() should return the stored item', () => {
    spyOn(localStorage, 'getItem').and.returnValue(fakeValue);

    const item = storageService.getItem(fakeKey);

    expect(item).toBe(fakeValue);
  });

  it('storageService.removeItem() should call  localStorage.removeItem() with correct parameters', () => {
    const removeItemSpy = spyOn(localStorage, 'removeItem');

    storageService.removeItem(fakeKey);

    expect(removeItemSpy).toHaveBeenCalledTimes(1);
    expect(removeItemSpy).toHaveBeenCalledWith(fakeKey);
  });

  it('storageService.clear() should call  localStorage.clear()', () => {
    const clearSpy = spyOn(localStorage, 'clear');

    storageService.clear();

    expect(clearSpy).toHaveBeenCalledTimes(1);
  });
});
