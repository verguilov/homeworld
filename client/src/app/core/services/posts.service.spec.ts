import { UsersService } from './users.service';
import { async, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { of } from 'rxjs';
import { PostsService } from './posts.service';
import { puts } from 'util';

describe('PostsService', () => {
  const FAKE_API_URL = 'http://localhost:3000/api';
  const fakeToken = 'authToken';
  let httpClient;
  let storageService;
  let authService;
  let usersService;
  let postsService: PostsService;


  beforeEach(async () => {
    httpClient = {
      get() { },
      post() { },
      delete() { },
      put() { }
    };

    usersService = {
        /* */
    };

    storageService = {
      getItem() { return fakeToken; },
      setItem() { },
      removeItem() { }
    };

    authService = {
      isTokenExpired() { },
      decodeToken() { }
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [PostsService, AuthService, StorageService, UsersService]
    })
      .overrideProvider(HttpClient, { useValue: httpClient })
      .overrideProvider(StorageService, { useValue: storageService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(UsersService, { useValue: usersService});

    postsService = TestBed.get(PostsService);
  });

  it('should be created', () => {
    expect(postsService).toBeTruthy();
  });

  describe(('findPosts() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeTake = 10;
      const fakeSkip = 20;
      const fakeUrl = `${FAKE_API_URL}/posts?skip=${fakeSkip}&take=${fakeTake}`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.findPosts(fakeSkip, fakeTake);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('findPublicPosts() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeTake = 10;
      const fakeSkip = 20;
      const fakeUrl = `${FAKE_API_URL}/posts/public?skip=${fakeSkip}&take=${fakeTake}`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.findPublicPosts(fakeSkip, fakeTake);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('findFollowersPosts() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeTake = 10;
      const fakeSkip = 20;
      const fakeId = 3;
      const fakeUrl = `${FAKE_API_URL}/posts/followers?skip=${fakeSkip}&take=${fakeTake}&id=${fakeId}`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.findFollowersPosts(fakeId, fakeSkip, fakeTake);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('findFollowingPosts() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeTake = 10;
      const fakeSkip = 20;
      const fakeId = 3;
      const fakeUrl = `${FAKE_API_URL}/posts/following?skip=${fakeSkip}&take=${fakeTake}&id=${fakeId}`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.findFollowingPosts(fakeId, fakeSkip, fakeTake);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('findPostById() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeId = 3;
      const fakeUrl = `${FAKE_API_URL}/posts/${fakeId}`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.findPostById(fakeId);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('createPost() should'), () => {
    it('call http.post() method with correct parameters', () => {
      const fakeFormData = new FormData;
      const fakeUrl = `${FAKE_API_URL}/posts`;
      const postSpy = spyOn(httpClient, 'post');

      postsService.createPost(fakeFormData);

      expect(postSpy).toHaveBeenCalledTimes(1);
      expect(postSpy).toHaveBeenCalledWith(fakeUrl, fakeFormData);
    });
  });

  describe(('editPost() should'), () => {
    it('call http.put() method with correct parameters', () => {
      const fakeId = 10;
      const fakeUpdatedPost = {
          title: 'title',
          description: 'description',
          status: 'status'
      };
      const fakeUrl = `${FAKE_API_URL}/posts/${fakeId}`;
      const putSpy = spyOn(httpClient, 'put');

      postsService.editPost(fakeId, fakeUpdatedPost);

      expect(putSpy).toHaveBeenCalledTimes(1);
      expect(putSpy).toHaveBeenCalledWith(fakeUrl, fakeUpdatedPost);
    });
  });

  describe(('deletePost() should'), () => {
    it('call http.delete() method with correct parameters', () => {
      const fakeId = 10;
      const fakeUrl = `${FAKE_API_URL}/posts/${fakeId}`;
      const deleteSpy = spyOn(httpClient, 'delete');

      postsService.deletePost(fakeId);

      expect(deleteSpy).toHaveBeenCalledTimes(1);
      expect(deleteSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });

  describe(('likePost() should'), () => {
    it('call http.post() method with correct parameters', () => {
      const fakeId = 10;
      const fakeLike = { liked: true };
      const fakeUrl = `${FAKE_API_URL}/posts/${fakeId}/likes`;
      const postSpy = spyOn(httpClient, 'post');

      postsService.likePost(fakeId, fakeLike);

      expect(postSpy).toHaveBeenCalledTimes(1);
      expect(postSpy).toHaveBeenCalledWith(fakeUrl, fakeLike);
    });
  });

  describe(('getLocation() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeUrl = `https://ipapi.co/json/`;
      const getSpy = spyOn(httpClient, 'get');

      postsService.getLocation();

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });
});
