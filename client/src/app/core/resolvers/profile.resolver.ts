import { UserDTO } from './../../models/user/user.dto';
import { UsersService } from './../services/users.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class ProfileResolver implements Resolve<UserDTO> {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    const userId = +route.paramMap.get('userId');

    return zip(
      this.usersService.findUserById(userId),
      this.usersService.findUserCreatedPosts(userId),
      this.usersService.findUserLikedPosts(userId),
      this.usersService.findUserFollowers(userId),
      this.usersService.findUserFollowing(userId),
    )
      .pipe(
        map(([user, createdPosts, likedPosts, following, followers]) =>
          ({ user, createdPosts, likedPosts, following, followers })
        ),
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
