import { PostDTO } from '../../models/post/post.dto';
import { PostsService } from '../services/posts.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PostsResolver implements Resolve<PostDTO[]> {
  constructor(
    private readonly postsService: PostsService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<PostDTO[]> {
    return this.postsService.findPosts(0, 6)
      .pipe(
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
