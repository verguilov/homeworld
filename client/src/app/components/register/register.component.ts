import { Router } from '@angular/router';
import { UserDTO } from './../../models/user/user.dto';
import { UsersService } from './../../core/services/users.service';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  registerForm: FormGroup;

  constructor(
    private readonly usersService: UsersService,
    private readonly fb: FormBuilder,
    private readonly router: Router,
  ) {
    this.buildForm();
  }

  register(): void {
    this.usersService.registerUser(this.registerForm.value)
      .subscribe((user: UserDTO) => {
        this.router.navigate(['/login']);
      });
  }

  private buildForm(): void {
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.maxLength(30),
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
      repeatPassword: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
    },
      { validator: this.checkPasswords }
    );
  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const repeatPassword = group.get('repeatPassword').value;

    return password === repeatPassword ? null : { notSame: true };
  }
}
