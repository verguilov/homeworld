import { UserLoginDTO } from './../../../../../server/src/models/user/user-login.dto';
import { NotificationService } from './../../core/services/notification.service';
import { StorageService } from './../../core/services/storage.service';
import { Router } from '@angular/router';
import { AuthService } from './../../core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly storageService: StorageService,
    private readonly notificationService: NotificationService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.loginForm = this.fb.group({
      email: [
        this.storageService.getItem('email') || '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(30),
        ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
    });
  }

  ngOnInit(): void {
  }

  login(): void {
    const userCredentials: UserLoginDTO = { password: this.loginForm.controls.password.value };
    userCredentials[this.isEmail(this.loginForm.controls.email.value) ? 'email' : 'username'] = this.loginForm.controls.email.value;

    this.authService.login(userCredentials)
      .subscribe(
        (data) => {
          this.router.navigate(['/home-world']);
          this.notificationService.success('Welcome :)');
        },
        () => this.notificationService.error('You have entered an invalid username or password'),
      );
  }

  private isEmail(email: string): boolean {
    // tslint:disable-next-line: max-line-length
    const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return emailPattern.test(email);
  }
}
