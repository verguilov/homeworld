import { SocketService } from '../../core/services/socket.service';
import { NotificationService } from './../../core/services/notification.service';
import { AuthService } from '../../core/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../models/user/user.dto';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
    private loggedUserSubscription: Subscription;
    loggedUserData: UserDTO;
    openSidebar = false;

    constructor(
        private readonly authService: AuthService,
        private readonly notificationService: NotificationService,
        private readonly webSocketService: SocketService,
        private readonly router: Router,
    ) { }

    sidebarToggle() {
        this.openSidebar = !this.openSidebar;
    };

    logout(): void {
        this.authService.logout();
    }

    ngOnInit(): void {
        this.loggedUserSubscription = this.authService.loggedUserData$
            .subscribe((loggedUserData: UserDTO) => {
                this.loggedUserData = loggedUserData;

                this.webSocketService.receiveNotification()
                    .subscribe((message: string) => this.notificationService.info(message));
            });
    }

    ngOnDestroy(): void {
        this.loggedUserSubscription.unsubscribe();
    }
}
