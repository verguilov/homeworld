import { NotificationService } from './../../core/services/notification.service';
import { AuthService } from './../../core/services/auth.service';
import { UsersService } from './../../core/services/users.service';
import { UserDTO } from './../../models/user/user.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  loggedUserData: UserDTO;
  users: UserDTO[] = [];
  keyword = '';
  take = 10;
  skip = 0;

  constructor(
    private readonly notificationService: NotificationService,
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
  ) { }

  onScroll(): void {
    if (this.skip <= this.users.length) {
      this.usersService.findUsers(this.keyword, this.skip, this.take)
        .subscribe((data: UserDTO[]) => {
          this.users = [...this.users, ...data];
          this.skip += this.take;
        });
    }
  }

  search(keyword: string): void {
    this.keyword = keyword;
    this.skip = 0;

    this.usersService.findUsers(this.keyword, this.skip, this.take)
      .subscribe((data: UserDTO[]) => {
        this.users = data;
      });
  }

  followUser(user: UserDTO): void {
    if (user.id === this.loggedUserData.id) {
      return;
    }

    this.usersService
      .followUser(this.loggedUserData.id, user.id)
      .subscribe(({ userFollower, userFollowed }) => {
        const followerIndex = this.users.findIndex(u => u.id === userFollower.id);
        const followedIndex = this.users.findIndex(u => u.id === userFollowed.id);

        this.users[followerIndex] = userFollower;
        this.users[followedIndex] = userFollowed;
        this.notificationService.success(`You have started following ${userFollowed.username}.`);
      });
  }

  unFollowUser(user: UserDTO): void {
    if (user.id === this.loggedUserData.id) {
      return;
    }

    this.usersService
      .unFollowUser(this.loggedUserData.id, user.id)
      .subscribe(({ userFollower, userFollowed }) => {
        const followerIndex = this.users.findIndex(u => u.id === userFollower.id);
        const followedIndex = this.users.findIndex(u => u.id === userFollowed.id);

        this.users[followerIndex] = userFollower;
        this.users[followedIndex] = userFollowed;
        this.notificationService.warning(`You are no longer following ${userFollowed.username}.`);
      });
  }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((loggedUserData: UserDTO) => {
        this.loggedUserData = loggedUserData;
      });

    this.route.data
      .subscribe(({ data }) => {
        this.users = data || [];
        this.skip = this.users.length;
      });
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }
}
