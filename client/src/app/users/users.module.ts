import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { UserInfoComponent } from './user-info/user-info.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersRoutingModule } from './users.routing';

@NgModule({
  declarations: [UserInfoComponent, UsersListComponent],
  imports: [
    SharedModule,
    UsersRoutingModule,
  ],
  exports: [UserInfoComponent, UsersListComponent],
})
export class UsersModule { }
