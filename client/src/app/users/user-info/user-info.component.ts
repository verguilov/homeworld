import { NotificationService } from './../../core/services/notification.service';
import { Router } from '@angular/router';
import { UserDTO } from './../../models/user/user.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  @Input() user: UserDTO;
  @Input() loggedUser: UserDTO;

  @Output() userFollow: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output() userUnFollow: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();

  constructor(
    private readonly notificationService: NotificationService,
    private readonly router: Router
  ) { }

  ngOnInit(): void { }

  followUser(): void {
    if (!this.loggedUser) {
      this.notificationService.info(`Please login if you want to follow ${this.user.username}.`)
      return;
    }

    this.userFollow.emit(this.user);
  }

  unFollowUser(): void {
    if (!this.loggedUser) {
      return;
    }

    this.userUnFollow.emit(this.user);
  }

  showProfile(userId: number) {
    if (!this.loggedUser) {
      this.notificationService.info(`Please login if you want to see ${this.user.username}'s profile.`)
      return;
    }

    this.router.navigate(['/profile', userId])
  }
}
