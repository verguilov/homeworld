export class CommentCreateDTO {
    postId: number;
    content: string;
}
