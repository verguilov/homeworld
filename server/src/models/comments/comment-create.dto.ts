import { IsString, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CommentCreateDTO {
  @IsString()
  @Length(0, 200)
  @ApiModelProperty()
  content: string;
}
