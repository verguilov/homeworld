import { IsString, Length, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CommentUpdateDTO {
  @IsString()
  @Length(0, 200)
  @IsOptional()
  @ApiModelProperty({ required: false })
  content: string;
}
