import { UserRole } from '../../common/enums/user-role.enum';
import { IsNumber, IsPositive, IsString, IsNotEmpty, Length, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserSessionDTO {
  @IsNumber()
  @IsPositive()
  @ApiModelProperty()
  id: number;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiModelProperty()
  email: string;

  @ApiModelProperty()
  role: UserRole;
}
