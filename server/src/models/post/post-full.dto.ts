import { PostStatus } from './../../common/enums/post-status.enum';
import { IsString, Length, IsNotEmpty, IsNumber, IsPositive, IsEnum, IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { UserDTO } from '../user/user.dto';

export class PostFullDTO {
  @IsNumber()
  @IsPositive()
  @ApiModelProperty()
  id: number;

  @IsString()
  @Length(0, 100)
  @ApiModelProperty()
  title: string;

  @IsString()
  @Length(0, 1000)
  @ApiModelProperty()
  description: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  photoLink: string;

  @IsEnum(PostStatus)
  @ApiModelProperty()
  status: PostStatus;

  @IsNumber()
  likes: number;

  @IsBoolean()
  liked: boolean;

  @IsString()
  location: string;

  @Exclude()
  photoDeleteHash: string;

  @Exclude()
  isDeleted: boolean;

  @ApiModelProperty()
  creator: UserDTO;
}
