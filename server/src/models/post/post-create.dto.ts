import { PostStatus } from './../../common/enums/post-status.enum';
import { IsString, Length, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class PostCreateDTO {
  @IsString()
  @Length(0, 100)
  @ApiModelProperty()
  title: string;

  @IsString()
  @Length(0, 1000)
  @ApiModelProperty()
  description: string;

  @IsEnum(PostStatus)
  @ApiModelProperty()
  status: PostStatus;

  @IsString()
  @ApiModelProperty()
  location: string;
}
