import { IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CommentLikeDTO {
  @ApiModelProperty()
  id: number;

  @IsBoolean()
  @ApiModelProperty()
  liked: boolean;
}
