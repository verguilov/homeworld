import { Exclude, Expose } from 'class-transformer';
import { MinLength, IsOptional, IsString, IsNotEmpty, IsNumberString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class SearchOptionsDTO {
  @Expose()
  @IsOptional()
  @ApiModelProperty({ required: false })
  keyword?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({ required: false })
  order?: 'ASC' | 'DESC';

  @Expose()
  @IsOptional()
  @IsNumberString()
  @IsNotEmpty()
  @ApiModelProperty({ required: false })
  take?: string;

  @Expose()
  @IsOptional()
  @IsNumberString()
  @IsNotEmpty()
  @ApiModelProperty({ required: false })
  skip?: string;
}
