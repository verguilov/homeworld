import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Post } from '../database/entities/post.entity';
import { User } from './../database/entities/user.entity';
import { UsersService } from './../users/users.service';
import { PostsService } from './posts.service';
import { MapperService } from './../core/services/mapper.service';
import { PhotoStorageService } from '../core/services/photo-storage.service';
import { PostLike } from '../database/entities/post-like.entity';
import { UserRole } from '../common/enums/user-role.enum';

describe('PostsService', () => {
  let service: any;

  let postsRepository: any;
  let usersRepository: any;
  let postLikesRepository: any;

  let photoStorageService: any;
  let mapper: any;
  let usersService: any;
  const options = {take: 10, skip: 0};
  const fakeData = [{ id: '3', title: 'test1', creator: 'test1', status: 'public' },
  { id: '4', title: 'test2', creator: 'test2', status: 'public' }];
  const sessionUser = { id: 3 };
  const fakePostToCreate = { title: 'A post', description: 'some new one', status: 'public', location: 'somewhere'};
  const fakePhoto = {};
  const fakePublicPost = {
  id: '3',
  title: 'A post',
  description: 'some new one',
  __creator__: {
    id: 3,
    username: 'test1'},
  status: 'public',
  location: 'somewhere',
  isDeleted: false,
  photoDeleteHash: 'some hash',
  };

  beforeEach(async () => {
    postsRepository = {
      find() {
      },
      findOne() {
        fakePublicPost['__creator__'].id = 3;
        return fakePublicPost;
      },
      create() {
        return fakePublicPost;
      },
      save() {
        return fakePublicPost;
      },
      update() {
        return { ...fakePublicPost,
        title: 'some new title'};
      },
    };

    postLikesRepository = {
      count() {
        return 1;
      },
    };

    usersRepository = {
      findOne() {
        const foundUser = {id: 3, username: 'test1', role: UserRole.Admin};

        foundUser['followers'] = [ {id: 1}, {id: 2}, {id: 4}];
        foundUser['following'] = [ {id: 5}, {id: 6}, {id: 7}];

        return foundUser;
      },
    };

    photoStorageService = {
      uploadPhoto() {
        const newPhoto = {
          photoLink: 'some link',
          photoDeleteHash: 'some hash',
        };
        return newPhoto;
      },

      deletePhoto() {
        /*empty*/
      },
    };
    mapper = {
      toPostDTO() {
        /*empty*/
      },

      toPostFullDTO() {
        /*empty*/
      },
    };
    usersService = {/*empty*/ };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostsService,
        { provide: getRepositoryToken(Post), useValue: postsRepository },
        { provide: getRepositoryToken(User), useValue: usersRepository },
        { provide: getRepositoryToken(PostLike), useValue: postLikesRepository },
        { provide: PhotoStorageService, useValue: photoStorageService },
        { provide: MapperService, useValue: mapper },
        { provide: UsersService, useValue: usersService },
      ],
    }).compile();

    service = module.get<PostsService>(PostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findPosts', () => {
    it('should call the *find* method of the repository', async () => {

      const spy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(Promise.resolve(fakeData));

      await service.findPosts(options);

      expect(postsRepository.find).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });
  });

  describe('findPublicPosts', () => {
    it('should call the *find* method of the repository', async () => {

      const spy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(Promise.resolve(fakeData));

      await service.findPublicPosts(options);

      expect(postsRepository.find).toHaveBeenCalledTimes(1);

      spy.mockClear();
    });
  });

  describe('findFollowersPosts', () => {
    it('should call the *findOne* method of the usersRepository', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      await service.findFollowersPosts(options, 3);

      expect(usersRepository.findOne).toHaveBeenCalledTimes(1);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should throw if user is not found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne')
        .mockReturnValue(null);

      try {
          await service.findFollowersPosts(options, 21);
        } catch (e) {
          expect(e['message']).toEqual('User not found!');
        }

      usersSpy.mockClear();
    });

    it('should return with an empty array if user has no followers', async () => {

      const usersSpy = jest
      .spyOn(usersRepository, 'findOne')
      .mockReturnValue({
        id: 3,
        followers: [],
      });

      expect(await service.findFollowersPosts(options, 3)).toEqual([]);

      usersSpy.mockClear();
    });

    it('should not throw if followers were found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      expect(() => service.findFollowersPosts(options, 3)).not.toThrow();

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should call the *find* method of the postsRepository', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      await service.findFollowersPosts(options, 3);

      expect(postsRepository.find).toHaveBeenCalledTimes(1);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should return an empty array if no posts were found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue([]);

      expect(await service.findFollowersPosts(options, 3)).toEqual([]);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });
  });

  describe('findFollowingPosts', () => {
    it('should call the *findOne* method of the usersRepository', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      await service.findFollowingPosts(options, 3);

      expect(usersRepository.findOne).toHaveBeenCalledTimes(1);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should throw if user is not found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne')
        .mockReturnValue(null);

      try {
          await service.findFollowingPosts(options, 21);
        } catch (e) {
          expect(e['message']).toEqual('User not found!');
        }

      usersSpy.mockClear();
    });

    it('should return with an empty array if user follows no one', async () => {

      const usersSpy = jest
      .spyOn(usersRepository, 'findOne')
      .mockReturnValue({
        id: 3,
        following: [],
      });

      expect(await service.findFollowingPosts(options, 3)).toEqual([]);

      usersSpy.mockClear();
    });

    it('should not throw if followed users were found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      expect(() => service.findFollowingPosts(options, 3)).not.toThrow();

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should call the *find* method of the postsRepository', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue(fakeData);

      await service.findFollowingPosts(options, 3);

      expect(postsRepository.find).toHaveBeenCalledTimes(1);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });

    it('should return an empty array if no posts were found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      const postsSpy = jest
        .spyOn(postsRepository, 'find')
        .mockReturnValue([]);

      expect(await service.findFollowingPosts(options, 3)).toEqual([]);

      usersSpy.mockClear();
      postsSpy.mockClear();
    });
  });
  describe('findPostById', () => {
    it('should call the *findOne* method of the repository', async () => {

      const id = 3;

      const postsSpy = jest
        .spyOn(postsRepository, 'findOne')
        .mockReturnValue(fakePublicPost);

      await service.findPostById(options, id);

      expect(postsRepository.findOne).toHaveBeenCalledTimes(1);

      postsSpy.mockClear();
    });

    it('should throw if post is not found', async () => {

      const usersSpy = jest
        .spyOn(postsRepository, 'findOne')
        .mockReturnValue(null);

      try {
          await service.findPostById(21);
        } catch (e) {
          expect(e['message']).toEqual('Post not found!');
        }

      usersSpy.mockClear();
    });
  });

  describe('createPost', () => {
    it('should call the *create* method of the postsRepository', async () => {

      const postsSpy = jest
        .spyOn(postsRepository, 'create');

      await service.createPost(fakePostToCreate, fakePhoto, 3);

      expect(postsRepository.create).toHaveBeenCalledTimes(1);

      postsSpy.mockClear();
    });

    it('should call the *findOne* method of the usersRepository', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne');

      await service.createPost(fakePostToCreate, fakePhoto, 3);

      expect(usersRepository.findOne).toHaveBeenCalledTimes(1);

      usersSpy.mockClear();
    });

    it('should throw if user is not found', async () => {

      const usersSpy = jest
        .spyOn(usersRepository, 'findOne')
        .mockReturnValue(null);

      try {
          await service.createPost(fakePostToCreate, fakePhoto, 21);
        } catch (e) {
          expect(e['message']).toEqual('User not found!');
        }

      usersSpy.mockClear();
    });

    it('should call the *save* method of the postsRepository', async () => {

      const postsSpy = jest
        .spyOn(postsRepository, 'save');

      await service.createPost(fakePostToCreate, fakePhoto, 3);

      expect(postsRepository.save).toHaveBeenCalledTimes(1);

      postsSpy.mockClear();
    });

    it('sould not throw if user is found and photo is uploaded', async () => {
      expect(async () => await service.createPost(fakePostToCreate, fakePhoto, 3)).not.toThrow();
    });
  });

  describe('update', () => {
    it('should call the *getValidPost* method of the service', async () => {

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      await service.updatePost(3, {title: 'some new title'}, 3);

      expect(service.getValidPost).toHaveBeenCalledTimes(1);

      validSpy.mockClear();
    });

    it('should call the *update* method of the postsRepository', async () => {

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      const postsSpy = jest
        .spyOn(postsRepository, 'update');

      await service.updatePost(3, {title: 'some new title'}, 3);

      expect(postsRepository.update).toHaveBeenCalledTimes(1);

      validSpy.mockClear();
      postsSpy.mockClear();
    });
  });

  describe('deletePost', () => {
    it('should call the *getValidPost* method of the service', async () => {

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      await service.deletePost(3, 3);

      expect(service.getValidPost).toHaveBeenCalledTimes(1);

      validSpy.mockClear();
    });

    it('should call the *deletePhoto* method of the photoStorageService, if the post has a delete hash', async () => {

      const photoSpy = jest
        .spyOn(photoStorageService, 'deletePhoto');

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      await service.deletePost(3, 3);

      expect(photoStorageService.deletePhoto).toHaveBeenCalledTimes(1);

      photoSpy.mockClear();
    });

    it('should call the *deletePhoto* method of the photoStorageService with the proper delete hash', async () => {

      const photoSpy = jest
        .spyOn(photoStorageService, 'deletePhoto');

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      await service.deletePost(3, 3);

      expect(photoStorageService.deletePhoto).toHaveBeenCalledWith(fakePublicPost.photoDeleteHash);

      photoSpy.mockClear();
    });

    it('should call the *update* method of the repository', async () => {

      const spy = jest
        .spyOn(postsRepository, 'update');

      const validSpy = jest
        .spyOn(service, 'getValidPost')
        .mockReturnValue(fakePublicPost);

      await service.deletePost(3, 3);

      expect(postsRepository.update).toHaveBeenCalledTimes(1);
      spy.mockClear();
    });
  });

  describe('getValidPost', () => {
    it('should call the *findOne* method of the postsRepository', async () => {

      const postsSpy = jest
        .spyOn(postsRepository, 'findOne');

      await service.getValidPost(sessionUser, 3);

      expect(postsRepository.findOne).toHaveBeenCalledTimes(1);

      postsSpy.mockClear();
    });

    it('should throw if post is not found', async () => {

      const usersSpy = jest
        .spyOn(postsRepository, 'findOne')
        .mockReturnValue(null);

      try {
          await service.getValidPost(sessionUser, 3);
        } catch (e) {
          expect(e['message']).toEqual('Post not found!');
        }

      usersSpy.mockClear();
    });

    it('should throw if authorization fails', async () => {

      try {
          await service.getValidPost({id: 2, role: UserRole.User}, 3);
        } catch (e) {
          expect(e['message']).toEqual('Not authorized!');
        }
    });

    it('should return the found post', async () => {

      expect(await service.getValidPost(sessionUser, 3)).toEqual(fakePublicPost);
    });
  });
});
