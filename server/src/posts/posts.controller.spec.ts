import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { TestingModule, Test } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { LikesService } from '../core/services/likes.service';
import { CommentsService } from '../core/services/comments.service';
import { UserRole } from '../common/enums/user-role.enum';
import { PostStatus } from '../common/enums/post-status.enum';

describe('PostsController', () => {
    let controller: PostsController;
    let postsService: any;
    let likesService: any;
    let commentsService: any;
    const options = { take: '15', skip: '0' };
    const fakeUser = { id: 5, username: 'test1', email: 'test@email.com', role: UserRole.Admin };
    const fakePublicPost = { id: 3, title: 'A title', description: 'A description', status: PostStatus.public, location: 'Somewhere' };
    const fakePublicPost2 = { id: 4, title: 'Another one', description: 'Another description', status: PostStatus.public, location: 'Elsewhere' };
    const fakePosts = [fakePublicPost, fakePublicPost2];
    const fakePhoto = {};
    const fakeLike = { id: 4, liked: true };
    const fakeComment = { id: 6, content: 'gg' };

    beforeEach(async () => {
        postsService = {
          findPosts() {
              return fakePosts;
          },

          findPublicPosts() {
              return fakePosts;
          },

          findPostById() {
              return fakePublicPost;
          },

          findFollowersPosts() {
              return fakePosts;
          },

          findFollowingPosts() {
            return fakePosts;
          },

          createPost() {
            return fakePublicPost;
          },

          updatePost() {
            return fakePublicPost;
          },

          deletePost() {
            return fakePublicPost;
          },
        };

        likesService = {
          findPostLike() {
            return true;
          },

          findAllPostLikes() {
            return 5;
          },

          likePost() {
            return fakeLike;
          },

          findCommentLike() {
            return true;
          },

          findAllCommentLikes() {
            return 6;
          },

          likeComment() {
            return fakeLike;
          },
        };

        commentsService = {
          findComments() {
            return [fakeComment];
          },

          createComment() {
            return fakeComment;
          },

          updateComment() {
            return fakeComment;
          },

          deleteComment() {
            return fakeComment;
          },
        };

        const module: TestingModule = await Test.createTestingModule({
          controllers: [PostsController],
          providers: [
            { provide: PostsService, useValue: postsService },
            { provide: LikesService, useValue: likesService },
            { provide: CommentsService, useValue: commentsService },
          ],
          imports: [
            PassportModule.register({ defaultStrategy: 'jwt' }),
          ],
        }).compile();

        controller = module.get<PostsController>(PostsController);
      });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    describe('findPosts', () => {
        it('should call the *findPosts* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'findPosts');

            await controller.findPosts(options);

            expect(postsService.findPosts).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findPosts* method from postsService', async () => {
            expect(await controller.findPosts(options)).toEqual(fakePosts);
        });
    });

    describe('findPublicPosts', () => {
        it('should call the *findPublicPosts* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'findPublicPosts');

            await controller.findPublicPosts(options);

            expect(postsService.findPublicPosts).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findPublicPosts* method from postsService', async () => {
            expect(await controller.findPublicPosts(options)).toEqual(fakePosts);
        });
    });

    describe('findPostById', () => {
        it('should call the *findPostById* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'findPostById');

            await controller.findPostById(3, fakeUser);

            expect(postsService.findPostById).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findPostById* method from postsService', async () => {
            expect(await controller.findPostById(3, fakeUser)).toEqual(fakePublicPost);
        });
    });

    describe('findFollowersPosts', () => {
        it('should call the *findFollowersPosts* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'findFollowersPosts');

            await controller.findFollowersPosts(options, fakeUser);

            expect(postsService.findFollowersPosts).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findFollowersPosts* method from postsService', async () => {
            expect(await controller.findFollowersPosts(options, fakeUser)).toEqual(fakePosts);
        });
    });

    describe('findFollowingPosts', () => {
        it('should call the *findFollowingPosts* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'findFollowingPosts');

            await controller.findFollowingPosts(options, fakeUser);

            expect(postsService.findFollowingPosts).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findFollowingPosts* method from postsService', async () => {
            expect(await controller.findFollowingPosts(options, fakeUser)).toEqual(fakePosts);
        });
    });

    describe('createPost', () => {
        it('should call the *createPost* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'createPost');

            await controller.createPost(fakePublicPost, fakePhoto, fakeUser);

            expect(postsService.createPost).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *createPost* method from postsService', async () => {
            expect(await controller.createPost(fakePublicPost, fakePhoto, fakeUser)).toEqual(fakePublicPost);
        });
    });

    describe('updatePost', () => {
        it('should call the *updatePost* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'updatePost');

            await controller.updatePost(3, fakePublicPost, fakeUser);

            expect(postsService.updatePost).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *updatePost* method from postsService', async () => {
            expect(await controller.updatePost(3, fakePublicPost, fakeUser)).toEqual(fakePublicPost);
        });
    });

    describe('deletePost', () => {
        it('should call the *deletePost* method from the postsService', async () => {
            const findSpy = jest
                .spyOn(postsService, 'deletePost');

            await controller.deletePost(3, fakeUser);

            expect(postsService.deletePost).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *deletePost* method from postsService', async () => {
            expect(await controller.deletePost(3, fakeUser)).toEqual(fakePublicPost);
        });
    });

    describe('findPostLike', () => {
        it('should call the *findPostLike* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'findPostLike');

            await controller.findPostLike(3, fakeUser);

            expect(likesService.findPostLike).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findPostLike* method from likesService', async () => {
            expect(await controller.findPostLike(3, fakeUser)).toEqual(true);
        });
    });

    describe('findAllPostLikes', () => {
        it('should call the *findAllPostLikes* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'findAllPostLikes');

            await controller.findAllPostLikes(3);

            expect(likesService.findAllPostLikes).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findAllPostLikes* method from likesService', async () => {
            expect(await controller.findAllPostLikes(3)).toEqual(5);
        });
    });

    describe('likePost', () => {
        it('should call the *likePost* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'likePost');

            await controller.likePost(3, fakeLike, fakeUser);

            expect(likesService.likePost).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *likePost* method from likesService', async () => {
            expect(await controller.likePost(3, fakeLike, fakeUser)).toEqual(fakeLike);
        });
    });

    describe('findCommentLike', () => {
        it('should call the *findCommentLike* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'findCommentLike');

            await controller.findCommentLike(3, 6, fakeUser);

            expect(likesService.findCommentLike).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findCommentLike* method from likesService', async () => {
            expect(await controller.findCommentLike(3, 6, fakeUser)).toEqual(true);
        });
    });

    describe('findAllCommentLikes', () => {
        it('should call the *findAllCommentLikes* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'findAllCommentLikes');

            await controller.findAllCommentLikes(3, 6);

            expect(likesService.findAllCommentLikes).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *findAllCommentLikes* method from likesService', async () => {
            expect(await controller.findAllCommentLikes(3, 6)).toEqual(6);
        });
    });

    describe('likePostComment', () => {
        it('should call the *likeComment* method from the likesService', async () => {
            const findSpy = jest
                .spyOn(likesService, 'likeComment');

            await controller.likePostComment(3, 6, fakeLike, fakeUser);

            expect(likesService.likeComment).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *likePostComment* method from likesService', async () => {
            expect(await controller.likePostComment(3, 6, fakeLike, fakeUser)).toEqual(fakeLike);
        });
    });

    describe('findPostComments', () => {
        it('should call the *findComments* method from the commentsService', async () => {
            const findSpy = jest
                .spyOn(commentsService, 'findComments');

            await controller.findPostComments(3, fakeUser, options);

            expect(commentsService.findComments).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *deletePost* method from commentsService', async () => {
            expect(await controller.findPostComments(3, fakeUser, options)).toEqual([fakeComment]);
        });
    });

    describe('createPostComment', () => {
        it('should call the *createComment* method from the commentsService', async () => {
            const findSpy = jest
                .spyOn(commentsService, 'createComment');

            await controller.createPostComment(3, fakeComment, fakeUser);

            expect(commentsService.createComment).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *deletePost* method from commentsService', async () => {
            expect(await controller.createPostComment(3, fakeComment, fakeUser)).toEqual(fakeComment);
        });
    });

    describe('updatePostComment', () => {
        it('should call the *updateComment* method from the commentsService', async () => {
            const findSpy = jest
                .spyOn(commentsService, 'updateComment');

            await controller.updatePostComment(6, fakeComment, fakeUser);

            expect(commentsService.updateComment).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *deletePost* method from commentsService', async () => {
            expect(await controller.updatePostComment(6, fakeComment, fakeUser)).toEqual(fakeComment);
        });
    });

    describe('deletePostComment', () => {
        it('should call the *deleteComment* method from the commentsService', async () => {
            const findSpy = jest
                .spyOn(commentsService, 'deleteComment');

            await controller.deletePostComment(6, fakeUser);

            expect(commentsService.deleteComment).toHaveBeenCalledTimes(1);

            findSpy.mockClear();
        });

        it('should return the result of the *deletePost* method from commentsService', async () => {
            expect(await controller.deletePostComment(6, fakeUser)).toEqual(fakeComment);
        });
    });
});
