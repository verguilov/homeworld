import { PostLikeDTO } from './../models/likes/post-like.dto';
import { CommentUpdateDTO } from './../models/comments/comment-update.dto';
import { CommentCreateDTO } from './../models/comments/comment-create.dto';
import { CommentsService } from './../core/services/comments.service';
import { LikesService } from './../core/services/likes.service';
import { PostUpdateDTO } from './../models/post/post-update.dto';
import {
  Controller,
  Get,
  Query,
  ValidationPipe,
  Post,
  UseGuards,
  Body,
  UseInterceptors,
  UploadedFile,
  UseFilters,
  Param,
  ParseIntPipe,
  Put,
  Delete,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitFile,
} from '@nestjs/swagger';

import { PostCreateDTO } from './../models/post/post-create.dto';
import { PostsService } from './posts.service';
import { PostDTO } from './../models/post/post.dto';
import { SearchOptionsDTO } from '../models/search-options.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserSession } from '../decorators/user-session.decorator';
import { UserSessionDTO } from '../models/user/user-session.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { HomeWorldApplicationExceptionFilter } from '../filters/home-world-application-exception.filter';
import { UserGuard } from '../guards/user.guard';
import { LikeDTO } from '../models/likes/like.dto';
import { CommentDTO } from '../models/comments/comment.dto';
import { PostFullDTO } from '../models/post/post-full.dto';
import { PostLike } from '../database/entities/post-like.entity';

@Controller('api/posts')
@ApiUseTags('posts')
@UseFilters(HomeWorldApplicationExceptionFilter)
export class PostsController {
  constructor(
    private readonly postsService: PostsService,
    private readonly likesService: LikesService,
    private readonly commentsService: CommentsService,
  ) { }

  @Get()
  async findPosts(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
  ): Promise<PostDTO[]> {
    return await this.postsService.findPosts(options);
  }

  @Get('public')
  async findPublicPosts(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
  ): Promise<PostDTO[]> {
    return await this.postsService.findPublicPosts(options);
  }

  @Get('followers')
  async findFollowersPosts(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<PostDTO[]> {
    return await this.postsService.findFollowersPosts(options, loggedUser ? loggedUser.id : -1);
  }

  @Get('following')
  async findFollowingPosts(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<PostDTO[]> {
    return await this.postsService.findFollowingPosts(options, loggedUser ? loggedUser.id : -1);
  }

  @Get(':postId')
  async findPostById(
    @Param('postId', new ParseIntPipe()) postId: number,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<PostFullDTO> {
    return await this.postsService.findPostById(postId, loggedUser ? loggedUser.id : -1);
  }

  @Post()
  @UseGuards(AuthGuard(), UserGuard)
  @UseInterceptors(FileInterceptor('photo'))
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'photo', required: false, description: 'post photo' })
  async createPost(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) post: PostCreateDTO,
    @UploadedFile() photo: any,
    @UserSession() user: UserSessionDTO,
  ): Promise<PostDTO> {
    return this.postsService.createPost(post, photo, user.id);
  }

  @Put(':postId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  async updatePost(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) post: PostUpdateDTO,
    @UserSession() user: UserSessionDTO,
  ): Promise<PostFullDTO> {
    return await this.postsService.updatePost(postId, post, user);
  }

  @Delete(':postId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  async deletePost(
    @Param('postId', new ParseIntPipe()) postId: number,
    @UserSession() user: UserSessionDTO,
  ): Promise<PostDTO> {
    return await this.postsService.deletePost(postId, user);
  }

  @Get(':postId/likes')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async findPostLike(
    @Param('postId', new ParseIntPipe()) postId: number,
    @UserSession() user: UserSessionDTO,
  ): Promise<boolean> {
    return await this.likesService.findPostLike(postId, user.id);
  }

  @Get(':postId/likes/all')
  async findAllPostLikes(
    @Param('postId', new ParseIntPipe()) postId: number,
  ): Promise<number> {
    return await this.likesService.findAllPostLikes(postId);
  }

  @Post(':postId/likes')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async likePost(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) like: PostLikeDTO,
    @UserSession() user: UserSessionDTO,
  ): Promise<PostLikeDTO> {
    return await this.likesService.likePost(postId, user.id, like);
  }

  @Get(':postId/comments')
  async findPostComments(
    @Param('postId', new ParseIntPipe()) postId: number,
    @UserSession() loggedUser: UserSessionDTO,
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
  ): Promise<CommentDTO[]> {
    return await this.commentsService.findComments(postId, loggedUser ? loggedUser.id : -1, options);
  }

  @Post(':postId/comments')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async createPostComment(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) comment: CommentCreateDTO,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<CommentDTO> {
    return await this.commentsService.createComment(postId, loggedUser.id, comment);
  }

  @Put(':postId/comments/:commentId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  async updatePostComment(
    @Param('commentId', new ParseIntPipe()) commentId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) comment: CommentUpdateDTO,
    @UserSession() user: UserSessionDTO,
  ): Promise<CommentDTO> {
    return await this.commentsService.updateComment(commentId, comment, user);
  }

  @Delete(':postId/comments/:commentId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  async deletePostComment(
    @Param('commentId', new ParseIntPipe()) commentId: number,
    @UserSession() user: UserSessionDTO,
  ): Promise<CommentDTO> {
    return await this.commentsService.deleteComment(commentId, user);
  }

  @Get(':postId/comments/:commentId/likes')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async findCommentLike(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Param('commentId', new ParseIntPipe()) commentId: number,
    @UserSession() user: UserSessionDTO,
  ): Promise<boolean> {
    return await this.likesService.findCommentLike(postId, commentId, user.id);
  }

  @Get(':postId/comments/:commentId/likes/all')
  async findAllCommentLikes(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Param('commentId', new ParseIntPipe()) commentId: number,
  ): Promise<number> {
    return await this.likesService.findAllCommentLikes(postId, commentId);
  }

  @Post(':postId/comments/:commentId/likes')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async likePostComment(
    @Param('postId', new ParseIntPipe()) postId: number,
    @Param('commentId', new ParseIntPipe()) commentId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) like: LikeDTO,
    @UserSession() user: UserSessionDTO,
  ) {
    return await this.likesService.likeComment(postId, commentId, user.id, like);
  }
}
