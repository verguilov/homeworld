import { PostLike } from './../database/entities/post-like.entity';
import { Comment } from './../database/entities/comment.entity';
import { User } from './../database/entities/user.entity';
import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ConfigModule } from '../config/config.module';
import { CoreModule } from '../core/core.module';
import { Post } from '../database/entities/post.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Post, User, PostLike]),
    AuthenticationModule,
    ConfigModule,
    CoreModule,
  ],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule { }
