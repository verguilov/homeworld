import { Controller, Get, Redirect } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';

@Controller()
@ApiUseTags('root')
export class AppController {
  @Get()
  @Redirect('api')
  root(): void {}
}
