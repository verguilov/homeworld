import { InvalidUserCredentialsError } from '../common/errors/invalid-user-credentials.error';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './jwt-payload';
import { UserRole } from '../common/enums/user-role.enum';

@Injectable()
export class AuthenticationService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) { }

  public async login(user: UserLoginDTO): Promise<{ authToken: string }> {
    if (!user.username && !user.email) {
      throw new InvalidUserCredentialsError();
    }

    const loginMethod = user.email
      ? { email: user.email }
      : { username: user.username };

    const foundUser: User = await this.usersRepository
      .findOne({ where: { ...loginMethod, isDeleted: false } });

    if (!foundUser || !(await bcrypt.compare(user.password, foundUser.password))) {
      throw new InvalidUserCredentialsError();
    }

    const payload: JwtPayload = {
      id: foundUser.id,
      username: foundUser.username,
      email: foundUser.email,
      photoLink: foundUser.photoLink,
      role: UserRole[foundUser.role.type],
    };

    const authToken: string = await this.jwtService.signAsync(payload);

    return { authToken };
  }
}
