import { Injectable } from '@nestjs/common';
import { WebSocketGateway, SubscribeMessage } from '@nestjs/websockets';
import { Socket } from 'socket.io';

@Injectable()
@WebSocketGateway()
export class NotificationGateway {
  private users = {};

  @SubscribeMessage('leave')
  onLeave(connection: Socket, username: string) {
    if (this.users.hasOwnProperty(username)) {
      delete this.users[username];
    }
  }

  @SubscribeMessage('join')
  onJoin(connection: Socket, username: string) {
    connection.leaveAll();
    this.users[username] = connection;
    this.users[username].join(username);
  }

  @SubscribeMessage('notification')
  sendNotification(_: Socket, sender: string, receiver: string, message: string) {

    if (this.users.hasOwnProperty(sender) && this.users.hasOwnProperty(receiver)) {
      this.users[sender].to(receiver).emit('notification', message);
    }
  }
}
