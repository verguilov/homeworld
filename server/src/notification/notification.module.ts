import { NotificationGateway } from './notification.gateway';
import { Module } from '@nestjs/common';

@Module({
  providers: [NotificationGateway],
  exports: [NotificationGateway],
})
export class NotificationModule { }
