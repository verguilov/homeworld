import { NotificationModule } from './../notification/notification.module';
import { CommentLike } from './../database/entities/comment-like.entity';
import { PostLike } from './../database/entities/post-like.entity';
import { CommentsService } from './services/comments.service';
import { Comment } from './../database/entities/comment.entity';
import { LikesService } from './services/likes.service';
import { Post } from './../database/entities/post.entity';
import { User } from './../database/entities/user.entity';
import { ConfigModule } from './../config/config.module';
import { MapperService } from './services/mapper.service';
import { Module } from '@nestjs/common';
import { PhotoStorageService } from './services/photo-storage.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([PostLike, CommentLike, User, Post, Comment]),
    ConfigModule,
    NotificationModule,
  ],
  providers: [
    MapperService,
    PhotoStorageService,
    LikesService,
    CommentsService,
  ],
  exports: [
    MapperService,
    PhotoStorageService,
    LikesService,
    CommentsService,
  ],
})
export class CoreModule { }
