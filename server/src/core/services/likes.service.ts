import { CommentLikeDTO } from './../../models/likes/comment-like.dto';
import { PostLikeDTO } from './../../models/likes/post-like.dto';
import { MapperService } from './mapper.service';
import { CommentLike } from './../../database/entities/comment-like.entity';
import { PostLike } from './../../database/entities/post-like.entity';
import { NotFoundCommentError } from './../../common/errors/not-found-comment.error';
import { Comment } from './../../database/entities/comment.entity';
import { NotFoundPostError } from './../../common/errors/not-found-post.error';
import { NotFoundUserError } from './../../common/errors/not-found-user.error';
import { LikeDTO } from '../../models/likes/like.dto';
import { Post } from './../../database/entities/post.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { UserSessionDTO } from '../../models/user/user-session.dto';
import { NotificationGateway } from '../../notification/notification.gateway';

@Injectable()
export class LikesService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    @InjectRepository(Comment) private readonly commentsRepository: Repository<Comment>,
    @InjectRepository(PostLike) private readonly postLikesRepository: Repository<PostLike>,
    @InjectRepository(CommentLike) private readonly commentLikesRepository: Repository<CommentLike>,
    private readonly mapper: MapperService,
    private readonly notificationGateway: NotificationGateway,
  ) { }

  async likePost(postId: number, userId: number, like: LikeDTO): Promise<PostLikeDTO> {
    const [foundPost, foundUser, previousPostLike] = await Promise.all([
      this.postsRepository.findOne({ where: { id: postId, isDeleted: false }, relations: ['creator'] }),
      this.usersRepository.findOne({ where: { id: userId, isDeleted: false } }),
      this.postLikesRepository.findOne({
        where: {
          user: { id: userId, isDeleted: false },
          post: { id: postId, isDeleted: false },
          isDeleted: false,
        },
      }),
    ]);

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    if (!foundPost) {
      throw new NotFoundPostError();
    }

    const postLike: PostLike = previousPostLike
      ? previousPostLike
      : this.postLikesRepository.create();

    postLike.user = Promise.resolve(foundUser);
    postLike.post = Promise.resolve(foundPost);
    postLike.liked = like.liked;

    const newPostLike: PostLike = await this.postLikesRepository.save(postLike);

    if (postLike.liked) {
      const sender = foundUser.username;
      const reciever = foundPost['__creator__'].username;
      const message = `${sender} liked your post :)`;

      this.notificationGateway.sendNotification(
        null,
        sender,
        reciever,
        message,
      );
    }

    return this.mapper.toPostLikeDTO(newPostLike);
  }

  async likeComment(
    postId: number,
    commentId: number,
    userId: number,
    like: LikeDTO,
  ): Promise<CommentLikeDTO> {
    const [foundComment, foundUser, previousCommentLike] = await Promise.all([
      this.commentsRepository.findOne({
        where: {
          id: commentId,
          isDeleted: false,
          post: { id: postId, isDeleted: false },
        },
      }),
      this.usersRepository.findOne({
        where: { id: userId, isDeleted: false },
      }),
      this.commentLikesRepository.findOne({
        where: {
          post: postId,
          user: { id: userId },
          comment: { id: commentId },
        },
      }),
    ]);

    if (!foundComment) {
      throw new NotFoundCommentError();
    }

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const commentLike: CommentLike = previousCommentLike
      ? previousCommentLike
      : this.commentLikesRepository.create();

    commentLike.comment = Promise.resolve(foundComment);
    commentLike.user = Promise.resolve(foundUser);
    commentLike.liked = like.liked;

    const newCommentLike: CommentLike = await this.commentLikesRepository.save(commentLike);

    return this.mapper.toCommentLikeDTO(newCommentLike);
  }

  async findPostLike(postId: number, userId: number): Promise<boolean> {
    const foundPostLike: PostLikeDTO = await this.postLikesRepository.findOne({ where: { post: postId, user: userId } });

    if (foundPostLike) {
      return foundPostLike.liked;
    } else {
      return null;
    }
  }

  async findAllPostLikes(postId: number): Promise<number> {
    const foundLikes: PostLikeDTO[] = await this.postLikesRepository.find({ where: { post: postId, liked: true } });

    if (!foundLikes) {
      return null;
    } else {
      return foundLikes.length;
    }
  }

  async findCommentLike(postId: number, commentId: number, userId: number): Promise<boolean> {
    const foundCommentLike: PostLikeDTO = await this.commentLikesRepository.findOne({ where: { post: postId, comment: commentId, user: userId } });

    if (foundCommentLike) {
      return foundCommentLike.liked;
    } else {
      return null;
    }
  }

  async findAllCommentLikes(postId: number, commentId: number): Promise<number> {
    const foundLikes: PostLikeDTO[] = await this.commentLikesRepository.find({ where: { post: postId, comment: commentId, liked: true } });

    if (!foundLikes) {
      return null;
    } else {
      return foundLikes.length;
    }
  }
}
