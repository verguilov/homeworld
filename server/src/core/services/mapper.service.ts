import { CommentLikeDTO } from './../../models/likes/comment-like.dto';
import { CommentLike } from './../../database/entities/comment-like.entity';
import { PostLikeDTO } from './../../models/likes/post-like.dto';
import { PostLike } from './../../database/entities/post-like.entity';
import { CommentDTO } from './../../models/comments/comment.dto';
import { Comment } from './../../database/entities/comment.entity';
import { PostDTO } from './../../models/post/post.dto';
import { Post } from './../../database/entities/post.entity';
import { Injectable } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { plainToClass } from 'class-transformer';
import { UserDTO } from '../../models/user/user.dto';
import { PostFullDTO } from '../../models/post/post-full.dto';

@Injectable()
export class MapperService {
  toUserDTO(user: Partial<User>): UserDTO {
    return plainToClass(UserDTO, user, { excludePrefixes: ['_'] });
  }

  toPostDTO(post: Partial<Post>): PostDTO {
    return plainToClass(PostDTO, post, { excludePrefixes: ['_'] });
  }

  toPostFullDTO(post: Partial<Post>): PostFullDTO {
    return plainToClass(PostFullDTO, post, { excludePrefixes: ['_'] });
  }

  toCommentDTO(comment: Partial<Comment>): CommentDTO {
    return plainToClass(CommentDTO, comment, { excludePrefixes: ['_'] });
  }

  toFollowerDTO(follower: Partial<User>) {
    return follower;
  }

  toFollowedDTO(followed: Partial<User>) {
    return followed;
  }

  toPostLikeDTO(like: Partial<PostLike>) {
    return plainToClass(PostLikeDTO, like, { excludePrefixes: ['_'] });
  }

  toCommentLikeDTO(like: Partial<CommentLike>) {
    return plainToClass(CommentLikeDTO, like, { excludePrefixes: ['_'] });
  }
}
