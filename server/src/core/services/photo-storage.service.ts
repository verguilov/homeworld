import { InvalidImageFormat } from './../../common/errors/invalid-image-format.error';
import { ConfigService } from './../../config/config.service';
import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { extname } from 'path';
import { HomeWorldApplicationError } from '../../common/errors/home-world-application.error';

@Injectable()
export class PhotoStorageService {
  constructor(
    private readonly configService: ConfigService,
  ) { }

  async uploadPhoto(photo: any): Promise<{ photoLink: string, photoDeleteHash: string }> {
    if (!(/\.(gif|jpg|jpeg|png)$/i).test(extname(photo.originalname))) {
      throw new InvalidImageFormat();
    }

    const image = photo.buffer;

    const { data } = await axios(`${this.configService.photoStorageUrl}/upload`, {
      method: 'POST',
      headers: {
        'Authorization': this.configService.clientId,
        'Content-Type': 'multipart/form-data',
      },
      data: image,
    });

    return { photoLink: data.data.link, photoDeleteHash: data.data.deletehash };
  }

  async deletePhoto(photoDeleteHash: string): Promise<void> {
    await axios(`${this.configService.photoStorageUrl}/image/${photoDeleteHash}`, {
      method: 'DELETE',
      headers: {
        Authorization: this.configService.clientId,
      },
    });
  }
}
