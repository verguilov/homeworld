import { Comment } from './../entities/comment.entity';
import { CommentLike } from './../entities/comment-like.entity';
import { PostLike } from './../entities/post-like.entity';
import { createConnection } from 'typeorm';
import { User } from '../entities/user.entity';
import { Role } from '../entities/role.entity';
import { Post } from '../entities/post.entity';

const main = async () => {
  console.log('Cleaning data started...');

  const connection = await createConnection();
  const usersRepository = connection.getRepository(User);
  const rolesRepository = connection.getRepository(Role);
  const postsRepository = connection.getRepository(Post);
  const commentsRepository = connection.getRepository(Comment);
  const postLikesRepository = connection.getRepository(PostLike);
  const commentLikesRepository = connection.getRepository(CommentLike);

  await commentLikesRepository.delete({});
  await postLikesRepository.delete({});
  await commentsRepository.delete({});
  await postsRepository.delete({});
  await usersRepository.delete({});
  await rolesRepository.delete({});

  await connection.close();

  console.log('Data cleaned successfully!');
};

main().catch(console.error);
