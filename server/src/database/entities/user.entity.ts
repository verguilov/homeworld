import { CommentLike } from './comment-like.entity';
import { PostLike } from './post-like.entity';
import { Comment } from './comment.entity';
import { Post } from './post.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, RelationCount } from 'typeorm';
import { Role } from './role.entity';
import { DEFAULT_PROFILE_PHOTO_LINK } from '../../common/constants/default-pictures.const';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 20 })
  username: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 100 })
  email: string;

  @Column({ type: 'nvarchar', nullable: false })
  password: string;

  @Column({ type: 'nvarchar', default: DEFAULT_PROFILE_PHOTO_LINK })
  photoLink: string;

  @Column({ type: 'nvarchar', nullable: true })
  photoDeleteHash: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @ManyToOne(type => Role, role => role.type, { eager: true })
  role: Role;

  @OneToMany(type => Post, post => post.creator)
  createdPosts: Promise<Post[]>;

  @OneToMany(type => Comment, comment => comment.creator)
  createdComments: Promise<Comment[]>;

  @OneToMany(type => PostLike, postLike => postLike.user)
  postLikes: Promise<PostLike[]>;

  @OneToMany(type => CommentLike, commentLike => commentLike.user)
  commentLikes: Promise<CommentLike[]>;

  @ManyToMany(type => User, user => user.following)
  @JoinTable()
  followers: User[];

  @ManyToMany(type => User, user => user.followers)
  following: User[];

  @RelationCount((user: User) => user.followers)
  followersCount: number;

  @RelationCount((user: User) => user.following)
  followingCount: number;

  @RelationCount((user: User) => user.createdPosts)
  createdPostsCount: number;
}
