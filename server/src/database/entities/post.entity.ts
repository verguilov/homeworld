import { PostLike } from './post-like.entity';
import { Comment } from './comment.entity';
import { User } from './user.entity';
import { PostStatus } from './../../common/enums/post-status.enum';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, OneToMany } from 'typeorm';

@Entity('posts')
export class Post {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, length: 100 })
  title: string;

  @Column({ type: 'nvarchar', nullable: false, length: 1000 })
  description: string;

  @Column({ type: 'nvarchar', nullable: false })
  photoLink: string;

  @Column({ type: 'nvarchar', nullable: false })
  photoDeleteHash: string;

  @Column({ type: 'enum', enum: PostStatus, default: PostStatus.public })
  status: PostStatus;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @Column({ type: 'datetime', default: '01.01.2000' })
  creationDate: Date;

  @Column({ type: 'nvarchar', nullable: true })
  location: string;

  @ManyToOne(type => User, user => user.createdPosts)
  creator: Promise<User>;

  @OneToMany(type => Comment, comment => comment.post)
  comments: Promise<Comment[]>;

  @OneToMany(type => PostLike, postLike => postLike.post)
  likes: Promise<PostLike[]>;
}
