import { Post } from './post.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('post_likes')
export class PostLike {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'boolean', default: false })
  liked: boolean;

  @ManyToOne(type => User, user => user.postLikes)
  user: Promise<User>;

  @ManyToOne(type => Post, post => post.likes)
  post: Promise<Post>;
}
