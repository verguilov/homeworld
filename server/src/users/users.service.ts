import { PostLike } from './../database/entities/post-like.entity';
import { Post } from '../database/entities/post.entity';
import { UserSessionDTO } from './../models/user/user-session.dto';
import { NotAuthorizedError } from './../common/errors/not-authorized.error';
import { NotificationGateway } from './../notification/notification.gateway';
import { HomeWorldApplicationError } from './../common/errors/home-world-application.error';
import { UserFollowDTO } from './../models/user/user-follow.dto';
import { PhotoStorageService } from './../core/services/photo-storage.service';
import { UserUpdateDTO } from '../models/user/user-update.dto';
import { NotFoundUserError } from './../common/errors/not-found-user.error';
import { UserDTO } from '../models/user/user.dto';
import { MapperService } from './../core/services/mapper.service';
import { TakenEmailError } from './../common/errors/taken-email.error';
import { TakenUsernameError } from './../common/errors/taken-username.error';
import { UserCreateDTO } from '../models/user/user-create.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { UserRole } from '../common/enums/user-role.enum';
import { PostDTO } from '../models/post/post.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    @InjectRepository(PostLike) private readonly postLikesRepository: Repository<PostLike>,
    private readonly mapper: MapperService,
    private readonly photoStorage: PhotoStorageService,
    private readonly notificationGateway: NotificationGateway,
  ) { }

  async findUsers(options: any, loggedUserId: number): Promise<UserDTO[]> {
    const loggedUser: User = await this.usersRepository
      .findOne({
        where: {
          id: loggedUserId, isDeleted: false,
        },
        relations: ['following'],
      });

    const following = loggedUser ? loggedUser.following : [];

    const foundUsers: User[] = await this.usersRepository.find({
      where: { username: Like(`%${options.keyword || ''}%`), isDeleted: false },
      order: { username: options.order === 'DESC' ? 'DESC' : 'ASC' },
      take: options.take || '10',
      skip: options.skip || '0',
    });

    return foundUsers.map((u: User) => {
      return {
        ...this.mapper.toUserDTO(u),
        isFollowed: following
          ? following.some((f: User) => f.id === u.id)
          : false,
      };
    });
  }

  async findUserById(userId: number, loggedUserId: number): Promise<UserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['followers'] });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const isFollowed: boolean = loggedUserId !== -1 &&
      foundUser['followers'].some((u: User) => u.id === loggedUserId);

    return {
      ...this.mapper.toUserDTO({ ...foundUser }),
      isFollowed,
    };
  }

  async createUser(user: UserCreateDTO): Promise<UserDTO> {
    if ((await this.isTakenUsername(user.username))) {
      throw new TakenUsernameError();
    }

    if ((await this.isTakenEmail(user.email))) {
      throw new TakenEmailError();
    }

    const newUser: User = this.usersRepository.create({
      ...user,
      password: await bcrypt.hash(user.password, 10),
      role: await this.rolesRepository.findOne({ type: UserRole.User }),
      createdPosts: Promise.resolve([]),
      postLikes: Promise.resolve([]),
      commentLikes: Promise.resolve([]),
      followers: [],
      following: [],
    });

    const createdUser: User = await this.usersRepository.save(newUser);

    return this.mapper.toUserDTO(createdUser);
  }

  async updateUser(userId: number, loggedUser: UserSessionDTO, user: Partial<UserUpdateDTO>, photo?: any): Promise<UserDTO> {
    const foundUser: User = await this.usersRepository
      .findOne({ id: userId, isDeleted: false });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const canUpdate: boolean = loggedUser.role === UserRole.Admin ||
      (userId === loggedUser.id && (await bcrypt.compare(user.password, foundUser.password)));

    if (!canUpdate) {
      throw new NotAuthorizedError();
    }

    if (user.username && loggedUser.username !== user.username && (await this.isTakenUsername(user.username))) {
      throw new TakenUsernameError();
    }

    if (user.email && loggedUser.email !== user.email && (await this.isTakenEmail(user.email))) {
      throw new TakenUsernameError();
    }

    if (photo) {
      const { photoLink, photoDeleteHash } = await this.photoStorage.uploadPhoto(photo);

      const oldDeleteHash = foundUser.photoDeleteHash;

      foundUser.photoLink = photoLink;
      foundUser.photoDeleteHash = photoDeleteHash;

      if (oldDeleteHash) {
        await this.photoStorage.deletePhoto(oldDeleteHash);
      }
    }

    const updatedUser: User = { ...foundUser, ...user };

    updatedUser.password = user.newPassword
      ? await bcrypt.hash(user.newPassword, 10)
      : foundUser.password;

    await this.usersRepository.save(updatedUser);

    return this.mapper.toUserDTO(updatedUser);
  }

  async deleteUser(userId: number, userSession: UserSessionDTO): Promise<UserDTO> {
    const canDelete: boolean = userId === userSession.id ||
      userSession.role === UserRole.Admin;

    if (!canDelete) {
      throw new NotAuthorizedError();
    }

    const foundUser: User = await this.usersRepository
      .findOne({ where: { id: userId, isDeleted: false }, relations: ['createdPosts'] });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    await this.postsRepository.save(foundUser['__createdPosts__'].map((p: Post) => ({ id: p.id, isDeleted: true })));
    await this.usersRepository.save({ id: userId, isDeleted: true, followers: null, following: null });

    return this.mapper.toUserDTO(foundUser);
  }

  async followUser(userId: number, userSessionId: number, user: UserFollowDTO): Promise<{ userFollower: UserDTO, userFollowed: UserDTO }> {
    if (userId !== userSessionId) {
      throw new NotAuthorizedError();
    }

    if (userId === user.userId) {
      throw new HomeWorldApplicationError('You cannot follow yourself', 400);
    }

    const [userFollower, userFollowed] = await Promise.all([
      this.usersRepository.findOne({ where: { id: userId, isDeleted: false }, relations: ['following'] }),
      this.usersRepository.findOne({ where: { id: user.userId, isDeleted: false }, relations: ['followers'] }),
    ]);

    if (!userFollower || !userFollowed) {
      throw new NotFoundUserError();
    }

    if (user.follow && !userFollower.following.find(u => u.id === userFollowed.id)) {
      userFollowed.followers = [...userFollowed.followers, userFollower];
      userFollower.followingCount++;
      userFollowed.followersCount++;
      await this.usersRepository.save(userFollowed);

      const message = `${userFollower.username} started following you :)`;

      this.notificationGateway
        .sendNotification(null, userFollower.username, userFollowed.username, message);
    } else if (!user.follow && userFollower.following.find(u => u.id === userFollowed.id)) {
      userFollowed.followers = userFollowed.followers.filter(u => u.id !== userFollower.id);
      userFollower.followingCount--;
      userFollowed.followersCount--;

      await this.usersRepository.save(userFollowed);
    }

    return {
      userFollower: this.mapper.toUserDTO(userFollower),
      userFollowed: {
        ...this.mapper.toUserDTO(userFollowed),
        isFollowed: user.follow ? true : false,
      },
    };
  }

  async getUserFollowing(userId: number): Promise<UserDTO[]> {
    const foundUser: User = await this.usersRepository.findOne({
      where: { id: userId, isDeleted: false },
      relations: ['following'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    return foundUser['following'].map((u: User) => this.mapper.toUserDTO(u));
  }

  async getUserFollowers(userId: number): Promise<UserDTO[]> {
    const foundUser: User = await this.usersRepository.findOne({
      where: { id: userId, isDeleted: false },
      relations: ['followers'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    return foundUser['followers'].map((u: User) => this.mapper.toUserDTO(u));
  }

  async getUserPosts(userId: number, loggedUserId: number): Promise<PostDTO[]> {
    const foundUser = await this.usersRepository.findOne({
      where: { id: userId },
      relations: ['followers', 'createdPosts'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const showPrivate: boolean = userId === loggedUserId ||
      foundUser['followers'].some((u) => u.id === loggedUserId);

    if (showPrivate) {
      return foundUser['__createdPosts__']
        .filter((p: Post) => !p.isDeleted)
        .map((p: Post) => this.mapper.toPostDTO(p));
    }

    return foundUser['__createdPosts__']
      .filter((p: Post) => !p.isDeleted && p.status === 'public')
      .map((p: Post) => this.mapper.toPostDTO(p));
  }

  async getUserLikedPosts(userId: number, loggedUserId: number): Promise<PostDTO[]> {
    const foundUser = await this.usersRepository.findOne({
      where: { id: userId, isDeleted: false },
      relations: ['followers'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const showPrivate: boolean = userId === loggedUserId ||
      foundUser['followers'].some((u) => u.id === loggedUserId);

    const foundLikedPosts: PostDTO[] = (await this.postLikesRepository.find({
      where: {
        liked: true,
        user: { id: userId },
      },
      relations: ['post'],
    }))
      .filter((p: PostLike) => !p['__post__'].isDeleted)
      .map((p: PostLike) => this.mapper.toPostDTO(p['__post__']));

    return foundLikedPosts;
  }

  private async isTakenUsername(username: string): Promise<boolean> {
    return (await this.usersRepository.count({ username })) > 0;
  }

  private async isTakenEmail(email: string): Promise<boolean> {
    return (await this.usersRepository.count({ email })) > 0;
  }
}
