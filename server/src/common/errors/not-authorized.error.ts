import { HomeWorldApplicationError } from './home-world-application.error';

export class NotAuthorizedError extends HomeWorldApplicationError {
  constructor(message: string = 'Not authorized!') {
    super(message, 401);
  }
}
