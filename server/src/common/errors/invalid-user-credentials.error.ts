import { HomeWorldApplicationError } from './home-world-application.error';

export class InvalidUserCredentialsError extends HomeWorldApplicationError {
  constructor(message: string = 'Invalid username/email or password!') {
    super(message, 400);
  }
}
