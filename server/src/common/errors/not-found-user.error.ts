import { HomeWorldApplicationError } from './home-world-application.error';

export class NotFoundUserError extends HomeWorldApplicationError {
  constructor(message: string = 'User not found!') {
    super(message, 404);
  }
}
