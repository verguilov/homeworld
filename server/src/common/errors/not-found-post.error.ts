import { HomeWorldApplicationError } from './home-world-application.error';

export class NotFoundPostError extends HomeWorldApplicationError {
  constructor(message: string = 'Post not found!') {
    super(message, 404);
  }
}
