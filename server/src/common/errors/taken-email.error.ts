import { HomeWorldApplicationError } from './home-world-application.error';

export class TakenEmailError extends HomeWorldApplicationError {
  constructor(message: string = 'This email is already taken!') {
    super(message, 400);
  }
}
