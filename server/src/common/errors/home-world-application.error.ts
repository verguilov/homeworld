export class HomeWorldApplicationError extends Error {
  constructor(message: string, public code: number) {
    super(message);
  }
}
